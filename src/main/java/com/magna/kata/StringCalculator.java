package com.magna.kata;

public class StringCalculator {

    public int add(String numbers) {
        if (numbers==null){
         return -1;
        }

        if (numbers.length() == 0){
            return 0;
        }

        int[] intArray = convertToNumber(numbers);
        if (intArray.length == 1){
            return intArray[0];
        }

        return 0;
    }

    public int[] convertToNumber(String numbers){
        String[] stringArray = numbers.split(",");
        int[] intNumbers = new int[stringArray.length];

        for (int i = 0; i < stringArray.length; i++) {
            intNumbers[i] = Integer.parseInt(stringArray[i]);
        }
        return intNumbers;
    }
}
