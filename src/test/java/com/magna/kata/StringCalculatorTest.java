package com.magna.kata;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { StringCalculatorTestConfiguration.class })
public class StringCalculatorTest {

    @Test
    public void testValidInput() {
        StringCalculator stringCalculator = new StringCalculator();
        Assert.assertEquals(-1,stringCalculator.add(null));
    }

    @Test
    public void testEmptyInput(){
        StringCalculator stringCalculator = new StringCalculator();
        Assert.assertEquals(0,stringCalculator.add(""));
    }

    @Test
    public void testOneNumber(){
        StringCalculator stringCalculator = new StringCalculator();
        Assert.assertEquals(7,stringCalculator.add("7"));
    }

    @Test
    public void testTwoNumbers(){
        StringCalculator stringCalculator = new StringCalculator();
        Assert.assertEquals(12,stringCalculator.add("7,5"));
    }

//    @Test
//    public void testTwoDoubleDigitsNumbers(){
//        StringCalculator stringCalculator = new StringCalculator();
//        Assert.assertEquals(12,stringCalculator.add("7,5"));
//    }

}
